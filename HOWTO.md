# HOWTO add a new workflow 

## Create an issue
Create an [issue](https://forgemia.inra.fr/bios4biol/workflows/issues) with a description of your workflow.

## Create a branch

Clone the repository and create a branch corresponding to your workflow.

    git clone git@forgemia.inra.fr:bios4biol/workflows.git 
    git checkout -b develop-rnaseq

## Create your workflow

Copy the template workflow given as example in each workflow manager directory and edit files.

### For snakemake

Copy workflows/Snakemake/template_dev
required files are ...
#TODO

### For Nextflow

Copy workflows/Nextflow/template_dev

- main.nf:  the nextflow workflow files
- nextflow.config: the configuration of the workflow for genoclust
- README.md: description of the workflow (purpose, who is maintaining, how to use, ....), fill the provided example.
- test.sh: the command line to test the workflow. 

## Add test data
At root path of the repository you've got a data directory which include some test data. If available data cannot be used for your workflow add **little** example files.



