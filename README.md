# BIOS4BIOL workflow repository

This repository contains workflows (Snakemake/Nextflow):
 - developped by BIOS4BIOL members 
 - or retrieved from public repository and adapted for our purpose.

Those workflows are available on bioinfo Genotoul core facilities (SGE and/ or SLURM).

# Use snakemake on genoclust :

 ```
ssh user@genologin.toulouse.inra.fr
module load bioinfo/snakemake-4.5.1
snakemake -s /usr/local/bioinfo/workflows/Snakemake/anApplication/Snakefile \
           --cluster-config /usr/local/bioinfo/workflows/Snakemake/anApplication/ressources_SLURM.yaml \
           --drmaa " -q unlimitq --cpus-per-task={cluster.cpus} --time={cluster.time} --mem={cluster.mem} " --drmaa-log-dir your_slurm_log_dir
 ```
 
To rerun or restart a workflow, simply launch the exact same command.

More info about Snakemake available [here](http://snakemake.readthedocs.io)

More info about each workflow available into directory Snakemake


# Use nextflow on genoclust :

 ```
ssh user@genologin.toulouse.inra.fr
module load bioinfo/Nextflow-v0.27.3
nextflow run /usr/local/bioinfo/workflows/Nextflow/anApplication/main.nf -profile genoclust
 ```
Rerun/Restart a workflow :

 ```
 nextflow run /usr/local/bioinfo/workflows/Nextflow/anApplication/main.nf -profile genoclust -resume
 ```

More info about Nextflow available [here](https://www.nextflow.io/)

More info about each workflow available into directory Nextflow

# How to add a new workflow in this repository ?

Follow instructions writen in HOWTO.md file.

# Who is maintaining this repository ?

 - Maria Bernard -  maria.bernard@inra.fr -
 - Matthieu Charles - Mathieu.charles@inra.fr -
 - Sylvain Marthey - sylvain.marthey@inra.fr -
 - Céline Noirot - celine.noirot@inra.fr - [@cnoirot](https://twitter.com/cnoirot)

Please contact us if you want to participate.
