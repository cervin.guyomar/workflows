# coding: utf-8

__author__ = "Kenza BAZI KABBAJ - Sigenae"
__version__ = "1.0.0"

"""
Rule for variant calling with GATK haplotypeCaller tool. The outputs are zipped g.vcf file(s) per sample.
"""

rule haplotypeCaller:
    input:
        bam="results/recalibrated_bams/{sample}_dedup_recal.bam",
        ref=config["reference_genome"]
    output:
        protected("results/variantcalling/{sample}_dedup_recal.g.vcf.gz")
    threads: config["haplotypeCaller"]["cpu"]
    log: 
        stderr="results/variantcalling/logs/{sample}_calling.stderr",
        stdout="results/variantcalling/logs/{sample}_calling.stdout"

    params:
        mem=config["haplotypeCaller"]["mem"]
    shell:
        "java -Xmx{params.mem} -jar {config[gatk]} -T HaplotypeCaller -nct {threads} -R {input.ref} -I {input.bam} -o {output} -ERC GVCF -variant_index_type LINEAR -variant_index_parameter 128000 2> {log.stderr} > {log.stdout}"
