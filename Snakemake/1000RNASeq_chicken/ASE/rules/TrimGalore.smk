# coding: utf-8

__author__ = 'Maria BERNARD- Sigenae'
__version__ = '1.0.0'

'''
If necessary convert Phred64 to Phred33 and use trimGalore to remove adapters
'''

def get_phred64(wildcards):
    for f in table["forward_read"].tolist() + table["reverse_read"].dropna().tolist() :
        if re.search('^' + wildcards.prefix + '\.f.{0,3}q(\.gz){0,1}$',f):
            return config["data_dir"] + "/" + f
            
rule convertPhred:
    input:
        fastq_in = get_phred64
    output:
        fastq_out = temp("Results_ASE/Phred_convert/{prefix}.fastq.gz")
    script:
        "../script/convertPhred64_to_Phred33.py"


def get_fastq(wildcards):
    inputs = list()
    for f in table["forward_read"] :
        if re.search('^' + wildcards.prefix + '(_R1){0,1}\.f.{0,3}q(\.gz){0,1}$',f):
            is_phred64 = table[table["forward_read"] == f]["phred_scale"] == '64'
            if is_phred64.all():
                inputs.append("Results_ASE/Phred_convert/" + f)
            else:
                inputs.append(config["data_dir"] + "/" + f)
            r = table[table["forward_read"] == f]["reverse_read"]
            if not r.isnull().values.any():
                if is_phred64.all():
                    inputs.append("Results_ASE/Phred_convert/" + r.squeeze())
                else:
                    inputs.append(config["data_dir"] + "/" + r.squeeze())
    return inputs


rule trim_se:
    input:
        get_fastq
    output:
        out=temp("Results_ASE/TrimGalore/{prefix}_trim.fastq.gz")
    params:
        min_trim=lambda wildcards : str(int([ int(table[table["forward_read"] == f]["read_length"].tolist()[0]) for f in table["forward_read"] if f.startswith(wildcards.prefix)][0] / 3 )),
        qual=config["trimming_quality"]
    log:
        "Results_ASE/TrimGalore/{prefix}_trimSe.log"
    shell:
        """
        trim_galore --no_report_file --length {params.min_trim} --quality {params.qual} -o `dirname {output.out}` {input} 2> {log}
        mv `dirname {output.out}`/{wildcards.prefix}*trimmed.fq.gz {output.out}
        """


rule trim_pe:
    input:
        get_fastq
    output:
        out1=temp("Results_ASE/TrimGalore/{prefix}_trim_R1.fastq.gz"),
        out2=temp("Results_ASE/TrimGalore/{prefix}_trim_R2.fastq.gz")
    log:
        "Results_ASE/TrimGalore/{prefix}_trimPe.log"
    params:
        min_trim=lambda wildcards : str(int([ int(table[table["forward_read"] == f]["read_length"].tolist()[0]) for f in table["forward_read"] if f.startswith(wildcards.prefix)][0] / 3 )),
        qual=config["trimming_quality"]
    shell:
        """
        trim_galore --paired --no_report_file --length {params.min_trim} --quality {params.qual} -o `dirname {output.out1}` {input} 2> {log}
        mv `dirname {output.out1}`/{wildcards.prefix}*_val_1.fq.gz {output.out1}
        mv `dirname {output.out1}`/{wildcards.prefix}*_val_2.fq.gz {output.out2}
        """


def get_trim_Log(wildcards):
    inputs=list()
    for f in table["forward_read"]:
        prefix=re.split('(_R1){0,1}\.f.{0,3}q(\.gz){0,1}$', f)[0]
        r = table[table["forward_read"] == f]["reverse_read"]
        if not r.isnull().values.any():
            inputs.append("Results_ASE/TrimGalore/" + prefix + "_trimPe.log" )
        else:
            inputs.append("Results_ASE/TrimGalore/" + prefix + "_trimSe.log" )
    return inputs
    
    
rule log_merge_trim:
    input:
        logs=get_trim_Log
    output:
        out="Results_ASE/Summary/Log_TrimGalore_summary.tsv"
    script:
        "../script/trimgalore_summary.py"
        
