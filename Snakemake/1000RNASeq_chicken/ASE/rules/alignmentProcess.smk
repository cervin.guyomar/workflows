# coding: utf-8

__author__ = 'Maria BERNARD- Sigenae'
__version__ = '1.0.0'

'''
Align RNASeq reads on masked genome
'''

wildcard_constraints:
    properlyPaired = '_pp_|_'

STAR_INDEX_FILE=['genomeParameters.txt', 'chrName.txt', 'chrLength.txt', 'chrStart.txt', 'chrNameLength.txt', 'exonGeTrInfo.tab', 'geneInfo.tab', 'transcriptInfo.tab', 'exonInfo.tab', 'sjdbList.fromGTF.out.tab', 'sjdbInfo.txt', 'sjdbList.out.tab', 'Genome', 'SA', 'SAindex']

rule STAR_Index_1:
    input:
        fasta = os.path.abspath(config['fasta_ref']),
        gtf = os.path.abspath(config["gtf_ref"])
    output:
        expand('Results_ASE/STAR_Index_1/{file}', file=STAR_INDEX_FILE)
    threads: config["STAR_Index_1"]["cpu"]
    params:
        sjdbOverhang=max([int(x) for x in table.read_length]) - 1
    shell:
        "STAR --runThreadN {threads} --runMode genomeGenerate --genomeDir Results_ASE/STAR_Index_1  --genomeFastaFiles {input.fasta} --sjdbOverhang {params.sjdbOverhang} --sjdbGTFfile {input.gtf} "

def get_fastq_trim(wildcards):
    inputs = list()
    for f in table["forward_read"] :
        if re.search('^' + wildcards.prefix + '(_R1){0,1}\.f.{0,3}q(\.gz){0,1}$',f):
            inputs.append("Results_ASE/TrimGalore/" + f.replace(wildcards.prefix,wildcards.prefix + "_trim"))
            r = table[table["forward_read"] == f]["reverse_read"]
            if not r.isnull().values.any():
                inputs.append("Results_ASE/TrimGalore/" + r.squeeze().replace(wildcards.prefix,wildcards.prefix + "_trim"))
    return inputs

rule STAR_Aln_1:
    input:
        expand('Results_ASE/STAR_Index_1/{file}', file=STAR_INDEX_FILE[1:]),
        fastq = get_fastq_trim
    output:
        temp("Results_ASE/STAR_Aln_1/{prefix}_Log.out"), 
        temp("Results_ASE/STAR_Aln_1/{prefix}_Log.progress.out"),
        temp("Results_ASE/STAR_Aln_1/{prefix}_Log.final.out"),
        bam=temp("Results_ASE/STAR_Aln_1/{prefix}_Aligned.sortedByCoord.out.bam"),
        sj_out_tab="Results_ASE/STAR_Aln_1/{prefix}_SJ.out.tab"
    threads: config["STAR_Aln_1"]["cpu"]
    shell:
        "STAR --runThreadN {threads} --genomeDir Results_ASE/STAR_Index_1 --readFilesIn {input.fastq} --readFilesCommand zcat --outFileNamePrefix `dirname {output.bam}`/{wildcards.prefix}_ --outSAMtype BAM SortedByCoordinate --outFilterType BySJout"


# see https://github.com/alexdobin/STAR/issues/524 and https://groups.google.com/forum/#!searchin/rna-star/2-pass|sort:date/rna-star/Cpsf-_rLK9I/gq-DaeyvBAAJ
def get_SJout_STAR1(wildcards):
    inputs=list()
    forward_list = table["forward_read"]
    for f in forward_list:
        prefix=re.split('(_R1){0,1}\.f.{0,3}q(\.gz){0,1}$', f)[0]
        inputs.append("Results_ASE/STAR_Aln_1/" + prefix + "_SJ.out.tab" )
    return inputs

rule STAR_SJout_filter:
    input:
        sj_out_tab=get_SJout_STAR1
    output:
        'Results_ASE/STAR_Index_1/SJ.out.filtered.tab'
    shell:
        """
        # filter junctions on: mitochondria , non canonical, already known, covered by less than 3 uniquely mapped reads in at least 1 sample
        cat {input.sj_out_tab} | awk '$6==0 && $5>0 && $7>=2' | cut -f1-6 | sort | uniq -c |awk '{{if($1>1){{print $2,$3,$4,$5,$6,$7}}}}' |sed 's/ /\t/g' > {output}
        """

rule STAR_Aln_2:
    input:
        expand('Results_ASE/STAR_Index_1/{file}', file=STAR_INDEX_FILE[1:]),
        fastq = get_fastq_trim,
        sj_out_tab = 'Results_ASE/STAR_Index_1/SJ.out.filtered.tab'
    output:
        temp("Results_ASE/STAR_Aln_2/{prefix}_Log.out"),
        temp("Results_ASE/STAR_Aln_2/{prefix}_Log.progress.out"),
        temp("Results_ASE/STAR_Aln_2/{prefix}_SJ.out.tab"),
        g_bam=temp("Results_ASE/STAR_Aln_2/{prefix}_Aligned.sortedByCoord.out.bam"),
        log=temp("Results_ASE/STAR_Aln_2/{prefix}_Log.final.out"),
        g_stat="Results_ASE/STAR_Aln_2/{prefix}_Aligned.sortedByCoord.out.flagstat"
    threads: config["STAR_Aln_2"]["cpu"]
    priority : 50
    shell:
        """
        STAR --runThreadN {threads} --genomeDir Results_ASE/STAR_Index_1/ --sjdbFileChrStartEnd {input.sj_out_tab} --readFilesIn {input.fastq} --readFilesCommand zcat --outFileNamePrefix `dirname {output.g_bam}`/{wildcards.prefix}_ --outSAMtype BAM SortedByCoordinate
        samtools flagstat {output.g_bam} > {output.g_stat}
        """

def get_STAR_log(wildcards):
    inputs = list()
    for f in table["forward_read"]:
        prefix=re.split('(_R1){0,1}\.f.{0,3}q(\.gz){0,1}$', f)[0]
        inputs.append("Results_ASE/" + wildcards.dir + "/" + prefix + "_Log.final.out" )
    return inputs

rule log_merge_STAR:
    input:
        logs=get_STAR_log
    output:
        out="Results_ASE/Summary/Log_{dir}_summary.tsv"
    wildcard_constraints:
        dir='STAR_Aln_[12]'
    script:
        "../script/star_summary.py"

    # PICARD
    # java -jar picard.jar AddOrReplaceReadGroups I={input.bam} O={output.bam} RGID={params.idx} RGLB={params.name} RGPL={params.sequencer} RGPU="-" RGSM={params.name} 
    # GATK4:
rule AddOrReplaceReadGroups:
    input :
        bam="Results_ASE/STAR_Aln_2/{prefix}_Aligned.sortedByCoord.out.bam",
    output : 
        bam=temp("Results_ASE/STAR_Aln_2/{prefix}_Aligned.sortedByCoord_rg.bam")
    params :
        idx  = lambda wildcards : [ table[table["forward_read"] == f]["idx"].tolist()[0] for f in table["forward_read"] if f.startswith(wildcards.prefix)][0],
        name = lambda wildcards : [ table[table["forward_read"] == f]["sample_name"].tolist()[0] for f in table["forward_read"] if f.startswith(wildcards.prefix)][0],
        sequencer = lambda wildcards : [ table[table["forward_read"] == f]["sequencer"].tolist()[0] for f in table["forward_read"] if f.startswith(wildcards.prefix)][0],
        mem=str(int(config["AddOrReplaceReadGroups"]["mem"].replace("G","")) -4 )+"G" if int(config["AddOrReplaceReadGroups"]["mem"].replace("G","")) -4 > 1 else config["AddOrReplaceReadGroups"]["mem"] ,
    shell:
        """
        gatk --java-options "-Xmx{params.mem}" AddOrReplaceReadGroups -I {input.bam} -O {output.bam} --RGID {params.idx} --RGLB {params.name} --RGPL {params.sequencer} --RGPU "-" --RGSM {params.name} 
        """

def get_genomic_bam(wildcards):
    inputs = list()
    for f in table[table["sample_name"] == wildcards.sample]["forward_read"]:
        prefix=re.split('(_R1){0,1}\.f.{0,3}q(\.gz){0,1}$', f)[0]
        inputs.append("Results_ASE/STAR_Aln_2/" + prefix + "_Aligned.sortedByCoord_rg.bam" )
    return inputs
    
rule merge_bam : 
    input:
        bam = get_genomic_bam
    output:
        bam = temp("Results_ASE/STAR_Aln_2/{sample}_rg_genomic.bam")
    threads:  config["merge_bam"]["cpu"]
    shell:
        """
        a=`echo {input.bam} | wc -w `
        if [[ $a > 1 ]]
        then 
        samtools merge -@{threads} {output.bam} {input.bam}
        else
        echo 
        cp {input.bam} {output.bam}
        fi
        """

rule properly_paired:
    input :
        bam = "Results_ASE/STAR_Aln_2/{sample}_rg_genomic.bam"
    output:
        bam = temp("Results_ASE/STAR_Aln_2/{sample}_rg_genomic_pp.bam")
    shell:
        """
        samtools view -h -f 2 {input.bam} -bo {output.bam} 
        """

def get_properly_paired_bam(wildcards):

        sample_table = table[table["sample_name"] == wildcards.sample]
        # paired end, filter on properly paired
        if len(sample_table["forward_read"]) == len(sample_table["reverse_read"].dropna()) :
            return "Results_ASE/STAR_Aln_2/" + wildcards.sample + "_rg_genomic_pp.bam"
        # single end, no filter
        elif len(sample_table["reverse_read"].dropna() ) == 0:
            return "Results_ASE/STAR_Aln_2/" + wildcards.sample + "_rg_genomic.bam"


# note : This tool uses the READ_NAME_REGEX and the OPTICAL_DUPLICATE_PIXEL_DISTANCE options as the primary methods to identify and differentiate duplicate types. Set READ_NAME_REGEX to null to skip optical duplicate detection, e.g. for RNA-seq or other data where duplicate sets are extremely large and estimating library complexity is not an aim

    # PICARD
    # java -jar picard.jar MarkDuplicates I={input.bam} O={output.bam} M={output.metrics} CREATE_INDEX=false READ_NAME_REGEX=null REMOVE_DUPLICATES=true VALIDATION_STRINGENCY=SILENT 
    # GATK4:    
rule RmDuplicates:
    input:
        bam = get_properly_paired_bam
    output:
        bam = temp("Results_ASE/STAR_Aln_2/{sample}_rg_genomic{properlyPaired}rmdup.bam"),
        metrics = "Results_ASE/STAR_Aln_2/{sample}_rg_genomic{properlyPaired}.metrics.txt",
    params:
        mem=str(int(config["rmdup"]["mem"].replace("G","")) -4 )+"G" if int(config["rmdup"]["mem"].replace("G","")) -4 > 1 else config["rmdup"]["mem"] ,
    shell:
        """
        gatk --java-options "-Xmx{params.mem}" MarkDuplicates -I {input.bam} -O {output.bam} -M {output.metrics} --CREATE_INDEX false --READ_NAME_REGEX 'null' --REMOVE_DUPLICATES true --VALIDATION_STRINGENCY SILENT
        """

rule remove_multimap:
    input:
        bam = "Results_ASE/STAR_Aln_2/{sample}_rg_genomic{properlyPaired}rmdup.bam"
    output:
        bam = temp("Results_ASE/STAR_Aln_2/{sample}_rg_genomic{properlyPaired}rmdup_uniq.bam")
    shell:
        """
        samtools view -h -q 255 {input.bam} -bo {output.bam} 
        """
    
    # GATK3 :
    # java -jar GenomeAnalysisTK.jar -T SplitNCigarReads -R {input.fasta} -I {input.bam} -o {output.bam} --disable_bam_indexing -rf ReassignOneMappingQuality -RMQF 255 -RMQT 60 -U ALLOW_N_CIGAR_READS 
    # GATK4 : default behavior, see --skip-mapping-quality-transform option
rule SplitNCigarReads:
    input:
        fasta = config['fasta_ref'], 
        bam = "Results_ASE/STAR_Aln_2/{sample}_rg_genomic{properlyPaired}rmdup_uniq.bam",
        bai = "Results_ASE/STAR_Aln_2/{sample}_rg_genomic{properlyPaired}rmdup_uniq.bam.bai"
    output:
        bam = "Results_ASE/STAR_Aln_2/{sample}_rg_genomic{properlyPaired}rmdup_uniq_split.bam"
    params:
        mem=config["SplitNCigarReads"]["mem"],
    shell:
        """
        gatk --java-options "-Xmx{params.mem}" SplitNCigarReads -R {input.fasta} -I {input.bam} -O {output.bam} --create-output-bam-index false  
        """
    
