# coding: utf-8

__author__ = 'Maria BERNARD- Sigenae'
__version__ = '1.0.0'

'''
ASE counter with phASER and ASEReadCounter
'''

def get_splitNCigar_bam(wildcards):

    sample_table = table[table["sample_name"] == wildcards.sample]
    
    # paired end, filter on properly paired and rmdup
    if len(sample_table["forward_read"]) == len(sample_table["reverse_read"].dropna()) :
        return "Results_ASE/STAR_Aln_2/" + wildcards.sample + "_rg_genomic_pp_rmdup_uniq_split.bam"
    # single end, no filter
    elif len(sample_table["reverse_read"].dropna() ) == 0:
        return "Results_ASE/STAR_Aln_2/" + wildcards.sample + "_rg_genomic_rmdup_uniq_split.bam"
    else :
    	raise Exception(wildcards.sample + " is sequenced in pair end & single end mode\n 	This workflow do not accept this case!\n")

def get_splitNCigar_bai(wildcards):

    sample_table = table[table["sample_name"] == wildcards.sample]
    
    # paired end, filter on properly paired and rmdup
    if len(sample_table["forward_read"]) == len(sample_table["reverse_read"].dropna()) :
        return "Results_ASE/STAR_Aln_2/" + wildcards.sample + "_rg_genomic_pp_rmdup_uniq_split.bam.bai"
    # single end, no filter
    elif len(sample_table["reverse_read"].dropna() ) == 0:
        return "Results_ASE/STAR_Aln_2/" + wildcards.sample + "_rg_genomic_rmdup_uniq_split.bam.bai"
    else :
    	raise Exception(wildcards.sample + " is sequenced in pair end & single end mode\n 	This workflow do not accept this case!\n")

	# GATK3 :
	# java -jar GenomeAnalysisTK.jar -T ASEReadCounter -R {input.ref} -o {output} -I {input.bam} -sites {input.vcf}  -U ALLOW_N_CIGAR_READS --minMappingQuality {params.mappingQuality} --minBaseQuality {params.baseQuality}
	# GATK4 : ALLOW_N_CIGAR_READS seems to be useless now
rule ASEReadCounter:
	input:
		ref = config["fasta_ref"],
		vcf = 'Results_ASE/vcfFiltration/' + os.path.basename(config['vcf']).replace('.vcf.gz','_FsQdBiall.vcf.gz'),
		idx = 'Results_ASE/vcfFiltration/' + os.path.basename(config['vcf']).replace('.vcf.gz','_FsQdBiall.vcf.gz.tbi'),
		bam = get_splitNCigar_bam,
		bai = get_splitNCigar_bai
	output:
		cvs = 'Results_ASE/ASEReadCounter/{sample}_ASEReadCounter.csv'
	params:
		mem  = config["ASEReadCounter"]["mem"],
		mappingQuality = config['mappingQuality'],
   		baseQuality = config['baseQuality']
	shell:
		"""
		gatk --java-options "-Xmx{params.mem}" ASEReadCounter -R {input.ref} -O {output} -I {input.bam} --tmp-dir Results_ASE/ASEReadCounter --variant {input.vcf}   --min-mapping-quality {params.mappingQuality} --min-base-quality {params.baseQuality}
		"""

rule phASER:
	input:
		vcf = 'Results_ASE/vcfFiltration/' + os.path.basename(config['vcf']).replace('.vcf.gz','_FsQdBiall.vcf.gz'),
		idx = 'Results_ASE/vcfFiltration/' + os.path.basename(config['vcf']).replace('.vcf.gz','_FsQdBiall.vcf.gz.tbi'),
		bam = get_splitNCigar_bam,
		bai = get_splitNCigar_bai
	output:
		csv = 'Results_ASE/phASER/{sample}_phASER.allelic_counts.txt',
		hap_count = 'Results_ASE/phASER/{sample}_phASER.haplotypic_counts.txt'
	threads:  config["phASER"]["cpu"]
	params:
		mappingQuality = config['mappingQuality'],
   		baseQuality = config['baseQuality'],
   		paired_option = lambda wildcards : '1' if len(table[table['sample_name'] == wildcards.sample]['forward_read']) == len(table[table['sample_name'] == wildcards.sample]['reverse_read'].dropna()) else '0', 
   		idSeparator = '_' if config['id_separator'] == '' else config['id_separator']
	shell:
		"""
		python2.7 {config[bin_dir]}/phaser.py --vcf {input.vcf} --bam {input.bam} --sample {wildcards.sample} --threads {threads} --o Results_ASE/phASER/{wildcards.sample}_phASER --paired_end {params.paired_option} --unphased_vars 1 --gw_phase_vcf 1 --baseq {params.baseQuality} --mapq {params.mappingQuality} --id_separator {params.idSeparator}
		"""


rule gtf2bed:
	input:
		gtf = config['gtf_ref']
	output:
		bed = 'Results_ASE/ref_bed/' + os.path.basename(config['gtf_ref']).replace('.gtf', '.bed')
	script:
		"../script/gtf2bed.py"


rule phASER_gene_ae:
	input:
		hap_count = "Results_ASE/phASER/{sample}_phASER.haplotypic_counts.txt",
		bed = 'Results_ASE/ref_bed/' + os.path.basename(config['gtf_ref']).replace('.gtf', '.bed')
	output:
		gene_ae = "Results_ASE/phASER_gene_ae/{sample}_phASER.gene_ae.txt"
	params:
		idSeparator = '_' if config['id_separator'] == '' else config['id_separator']
	shell:
		"""
		python2.7 {config[bin_dir]}/phaser_gene_ae.py --haplotypic_counts {input.hap_count} --features {input.bed} --o {output} --id_separator {params.idSeparator}
		"""


rule phASER_expr_matrix:
	input:
		expand("Results_ASE/phASER_gene_ae/{sample}_phASER.gene_ae.txt", sample=table["sample_name"].unique()),
		bed = 'Results_ASE/ref_bed/' + os.path.basename(config['gtf_ref']).replace('.gtf', '.bed')
	output:
		bed = 'Results_ASE/phASER_pop/' + os.path.basename(config['vcf']).replace('.vcf.gz','_phASER.bed.gz'),
		phased_bed = 'Results_ASE/phASER_pop/' + os.path.basename(config['vcf']).replace('.vcf.gz','_phASER.gw_phased.bed.gz')
	params:
		gene_ae_dir = "Results_ASE/phASER_gene_ae/",
		out_prefix = 'Results_ASE/phASER_pop/' + os.path.basename(config['vcf']).replace('.vcf.gz','_phASER')
	threads: config["phASER_expr_matrix"]["cpu"]
	shell:
		"""
		python2.7 {config[bin_dir]}/phaser_expr_matrix.py --t {threads} --gene_ae_dir {params.gene_ae_dir} --feature {input.bed} --o {params.out_prefix}
		"""
