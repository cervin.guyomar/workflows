# coding: utf-8

__author__ = "Kenza BAZI KABBAJ / Maria BERNARD - Sigenae"
__version__ = "1.0.0"

"""
Rules for base quality recalibration using GATK BaseRecalibrator and PrintReads tools. It will reprocess quality score for each nucleotids in reads based on a set of known variants. In addition, AnalyzeCovariates tool is used to generates plots in order to visualize the effects of the recalibration process (before and after recalibration). The outputs are recalibrated bam files per sample and pdf statistic report per sample.
"""

rule BaseRecalibrator_1:
    input:
        "results/gatk_realignment/{sample}.rg.sort.md.real.bai",
        bam="results/gatk_realignment/{sample}.rg.sort.md.real.bam",
        ref=config["reference_genome"],
        known_variants=config["known_variants"]
        
    output:
        temp("results/gatk_recalibration/{sample}.before.recal.table")
        
    threads: config["BaseRecalibrator_1"]["cpu"]
    
    log: 
        stderr="results/gatk_recalibration/logs/{sample}_BaseRecalibrator_1.stderr",
        stdout="results/gatk_recalibration/logs/{sample}_BaseRecalibrator_1.stdout"
        
    params:
        mem= config["BaseRecalibrator_1"]["mem"]
        
    shell:
        "java -Xmx{params.mem} -jar {config[bin][gatk]} -T BaseRecalibrator -S SILENT --allow_potentially_misencoded_quality_scores -nct {threads} -R {input.ref} -I {input.bam} -knownSites:vcf {input.known_variants} -o {output} 2> {log.stderr} > {log.stdout}"

rule PrintReads:
    input:
        "results/gatk_realignment/{sample}.rg.sort.md.real.bai",
        bam="results/gatk_realignment/{sample}.rg.sort.md.real.bam",
        recal_table="results/gatk_recalibration/{sample}.before.recal.table",
        ref=config["reference_genome"]
        
    output:
        "results/gatk_recalibration/{sample}.rg.sort.md.real.recal.bai",
        bam="results/gatk_recalibration/{sample}.rg.sort.md.real.recal.bam"
        
    threads: config["PrintReads"]["cpu"]
    
    log: 
        stderr="results/gatk_recalibration/logs/{sample}_PrintReads.stderr",
        stdout="results/gatk_recalibration/logs/{sample}_PrintReads.stdout"
        
    params:
        mem=config["PrintReads"]["mem"]
        
    shell:
        "java -Xmx{params.mem} -jar {config[bin][gatk]} -T PrintReads --allow_potentially_misencoded_quality_scores  -nct {threads} -R {input.ref} -I {input.bam} -BQSR {input.recal_table} -o {output.bam} 2> {log.stderr} > {log.stdout}"

rule BaseRecalibrator_2:
    input:
        "results/gatk_realignment/{sample}.rg.sort.md.real.bai",
        bam="results/gatk_realignment/{sample}.rg.sort.md.real.bam",
        ref=config["reference_genome"],
        known_variants=config["known_variants"],
        recal_table="results/gatk_recalibration/{sample}.before.recal.table"
        
    output:
        temp("results/gatk_recalibration/{sample}.after.recal.table")
        
    threads: config["BaseRecalibrator_2"]["cpu"]
    
    log:
        stderr="results/gatk_recalibration/logs/{sample}_BaseRecalibrator_2.stderr",
        stdout="results/gatk_recalibration/logs/{sample}_BaseRecalibrator_2.stdout"
        
    params:
        mem= config["BaseRecalibrator_2"]["mem"]
        
    shell:
        "java -Xmx{params.mem} -jar {config[bin][gatk]} -T BaseRecalibrator -S SILENT --allow_potentially_misencoded_quality_scores -nct {threads} -R {input.ref} -I {input.bam} -knownSites:vcf {input.known_variants} -BQSR {input.recal_table} -o {output} 2> {log.stderr} > {log.stdout}"

rule AnalyzeCovariates:
    input:
        before_recal_table="results/gatk_recalibration/{sample}.before.recal.table",
        after_recal_table="results/gatk_recalibration/{sample}.after.recal.table",
        ref=config["reference_genome"]
        
    output:
        "results/gatk_recalibration/{sample}_recal_plots.pdf"
        
    log:
        stderr="results/gatk_recalibration/logs/{sample}_AnalyzeCovariates.stderr",
        stdout="results/gatk_recalibration/logs/{sample}_AnalyzeCovariates.stdout"
        
    params:
        mem= config["AnalyzeCovariates"]["mem"]
        
    shell: 
        "java -Xmx{params.mem} -jar {config[bin][gatk]} -T AnalyzeCovariates -R {input.ref} -before {input.before_recal_table} -after {input.after_recal_table} -plots {output} 2> {log.stderr} > {log.stdout}"
