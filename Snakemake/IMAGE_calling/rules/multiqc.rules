# coding: utf-8

__author__ = "Kenza BAZI KABBAJ / Maria BERNARD - Sigenae"
__version__ = "1.0.0"

"""
multiqc analysis: summarizes stats outputs into a single html report (fastqc, flagstats, qualimap, metrics (markduplicates), bcftools stats, plots (before and after recalibration)) 
"""

rule multiqc:
    input:
        expand("results/fastqc/{prefix}_fastqc.zip", prefix = all_files),
        expand("results/markduplicates/{sample}.rg.sort.md.metrics", sample=all_samples),
        expand("results/flagstat/{sample}.flagstat", sample=all_samples),
        expand("results/qualimap/{sample}/genome_results.txt", sample=all_samples),
        expand("results/qualimap/{sample}/raw_data_qualimapReport/coverage_histogram.txt", sample=all_samples),
        expand("results/qualimap/{sample}/raw_data_qualimapReport/genome_fraction_coverage.txt", sample=all_samples),
        expand("results/qualimap/{sample}/raw_data_qualimapReport/mapped_reads_gc-content_distribution.txt", sample=all_samples),
        expand("results/gatk_recalibration/{sample}.before.recal.table", sample=all_samples),
        expand("results/gatk_recalibration/{sample}.after.recal.table", sample=all_samples),
        expand("results/bcftools_stats/gatk/{sample}_gatk.stats", sample=all_samples),
        expand("results/bcftools_stats/freebayes/{sample}_freebayes.stats", sample=all_samples),
        expand("results/bcftools_stats/mpileup/{sample}_mpileup.stats", sample=all_samples)
        
    output:
        "results/multiqc/multiqc_report.html"
        
    log:
        "results/multiqc/logs/multiqc.stderr"
        
    shell:
        "{config[bin][multiqc]} {input} -o results/multiqc/ -n multiqc_report 2> {log}"
