# coding: utf-8

__author__ = "Kenza BAZI KABBAJ / Maria BERNARD- Sigenae"
__version__ = "1.0.0"

"""
Rules to filtered genotypes called from tool little or too much reads
"""

rule genoFilter:
	input:
		sample_cov = config["sample_mean_cov"],
		vcf = "results/VQSR/{GATK_input_prefix}_vqsr_{var}.vcf.gz"
	output:
		vcf = "results/genoFilter/{GATK_input_prefix}_vqsr_{var}_genFiltered.vcf.gz"
	shell:
		"""
		filter_multi_sample_vcf.py --vcf_file {input.vcf} --bam_coverage {input.sample_cov} --output_vcf {output.vcf}
		"""
