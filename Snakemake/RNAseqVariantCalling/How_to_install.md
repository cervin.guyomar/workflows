## Install snakemake 5 with a venv

```
module load system/Python-3.6.3
python3.6 -m venv env/snakemake-5_venv
module purge
source env/snakemake-5_venv/bin/activate
pip install snakemake
pip install drmaa
pip install pandas
```

## Install conda and singularity

Conda:
```
wget https://repo.continuum.io/miniconda/Miniconda2-4.6.14-Linux-x86_64.sh
sh Miniconda2-4.6.14-Linux-x86_64.sh
```

Restart shell:
```
conda config --set auto_activate_base false
conda config --add channels bioconda
```

Singularity:
```
sudo apt-get install singularity-container
```

## Create singularity image

[see build process](./env/README.md)