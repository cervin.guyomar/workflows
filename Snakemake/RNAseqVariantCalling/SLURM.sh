#!/bin/bash
##############################################################################
## launch workflow

# if used on Genologin cluster (INRA Toulouse )
# 	module load system/Python-3.6.3

# Path to the SNAKEMAKE workflow directory
WORKFLOW_DIR=.

mkdir -p logs

# This is an example of the snakemake command line to launch a dryrun
snakemake -s $WORKFLOW_DIR/Snakefile_calling --printshellcmds --jobs 200 \
           --configfile config_calling.yaml.example --dryrun

# This is an example of the snakemake command line to launch the full workflow on a SLURM cluster
	# {cluster.cpu}, {cluster.mem} and {cluster.partition} will be replace by value defined in the resources_SLURM.yaml file
snakemake -s $WORKFLOW_DIR/Snakefile_calling --printshellcmds --jobs 200 \
           --configfile config_calling.yaml.example \
           --cluster-config $WORKFLOW_DIR/resources_SLURM.yaml \
           --cluster "sbatch -p {cluster.partition} --cpus-per-task={cluster.cpu} --mem-per-cpu={cluster.mem} --error=logs/%x.stderr --output=logs/%x.stdout " --latency-wait 30

# If you want to launch the workflow partially, indicate the final output files you want (see dryrun log)
# 
# For RSEM quantification only
PREFIX=??		# your sample_config TSV file whithout extension
snakemake -s $WORKFLOW_DIR/Snakefile_calling --printshellcmds --jobs 200 \
           --configfile config_calling.yaml.example \
           --cluster-config $WORKFLOW_DIR/resources_SLURM.yaml \
           --cluster "sbatch -p {cluster.partition} --cpus-per-task={cluster.cpu} --mem-per-cpu={cluster.mem} --error=logs/%x.stderr --output=logs/%x.stdout " --latency-wait 30 \
           Results/Summary/Log_TrimGalore_summary.tsv Results/Summary/Log_STAR_Aln_1_summary.tsv Results/Summary/Log_STAR_Aln_2_summary.tsv \
            Results/Summary/RSEM_multimap_summary_${PREFIX}_genes_expected_count.tsv \
            Results/Summary/RSEM_uniqmap_summary_${PREFIX}_genes_expected_count.tsv \
			Results/Summary/RSEM_multimap_summary_${PREFIX}_genes_FPKM.tsv \
			Results/Summary/RSEM_uniqmap_summary_${PREFIX}_genes_FPKM.tsv \
			Results/Summary/RSEM_multimap_summary_${PREFIX}_genes_TPM.tsv \
			Results/Summary/RSEM_uniqmap_summary_${PREFIX}_genes_TPM.tsv \
			Results/Summary/RSEM_multimap_summary_${PREFIX}_isoforms_expected_count.tsv \
			Results/Summary/RSEM_uniqmap_summary_${PREFIX}_isoforms_expected_count.tsv \
			Results/Summary/RSEM_multimap_summary_${PREFIX}_isoforms_FPKM.tsv \
			Results/Summary/RSEM_uniqmap_summary_${PREFIX}_isoforms_FPKM.tsv \
			Results/Summary/RSEM_multimap_summary_${PREFIX}_isoforms_TPM.tsv \
			Results/Summary/RSEM_uniqmap_summary_${PREFIX}_isoforms_TPM.tsv

# For GATK calling only
PREFIX=??		# your sample_config TSV file whithout extension
snakemake -s $WORKFLOW_DIR/Snakefile_calling --printshellcmds --jobs 200 \
           --configfile config_calling.yaml.example \
           --cluster-config $WORKFLOW_DIR/resources_SLURM.yaml \
           --cluster "sbatch -p {cluster.partition} --cpus-per-task={cluster.cpu} --mem-per-cpu={cluster.mem} --error=logs/%x.stderr --output=logs/%x.stdout " --latency-wait 30 \
           Results/Summary/Log_TrimGalore_summary.tsv Results/Summary/Log_STAR_Aln_1_summary.tsv Results/Summary/Log_STAR_Aln_2_summary.tsv \
           Results/GATK/${PREFIX}_SNP.vcf.gz\
           Results/GATK/${PREFIX}_INDEL.vcf.gz
